import { uuid } from 'uuidv4'

export const state = () => ({
  currentId: ''
})

export const mutations = {
  setCurrentId (state, { id }) {
    state.currentId = id
  }
}

export const actions = {
  /**
   * Shortcut action that create snippet book (a book with only document with a special name), set it as current book, and returns new book and document IDs.
   *
   * @returns {{bookId: string, documentId: string}}
   */
  async createAndSetSnippetBook ({ commit, dispatch }) {
    // Create book with random ID and first empty document
    const bookId = uuid()
    const documentId = uuid()
    
    // set book
    commit('setCurrentId', { id: bookId })

    return { bookId, documentId }
  }
}
