export default {
  // Disable server-side rendering (https://go.nuxtjs.dev/ssr-mode)
  ssr: false,

  // Target (https://go.nuxtjs.dev/config-target)
  target: 'static',

  // Global page headers (https://go.nuxtjs.dev/config-head)
  head: {
    title: 'Documatt Snippets, online reStructuredText and Sphinx editor',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      {
        hid: 'description',
        name: 'description',
        content: 'Snippets are place to play and learn how to write documentation and books with reStructuredText and Sphinx. You write content in reStructuredText syntax and Snippets render it with Sphinx.'
      }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },

  // Customize the progress-bar color
  loading: {
    height: '5px',
    color: '#1d75b3',
    continuous: true
  },

  // Global CSS (https://go.nuxtjs.dev/config-css)
  css: [
  ],

  // Plugins to run before rendering page (https://go.nuxtjs.dev/config-plugins)
  plugins: [
  ],

  // Auto import components (https://go.nuxtjs.dev/config-components)
  components: false,

  // Modules for dev and build (recommended) (https://go.nuxtjs.dev/config-modules)
  buildModules: [
    // https://go.nuxtjs.dev/eslint
    // '@nuxtjs/eslint-module'
  ],

  // Modules (https://go.nuxtjs.dev/config-modules)
  modules: [
    // https://go.nuxtjs.dev/buefy
    'nuxt-buefy',
    // https://go.nuxtjs.dev/axios
    '@nuxtjs/axios'
  ],

  // Build Configuration (https://go.nuxtjs.dev/config-build)
  build: {
    // You can extend webpack config here
    extend (config, ctx) {
      // Allows import file as a string ('import example from example.txt')
      config.module.rules.push({
        test: /\.(txt|rst)$/i,
        use: 'raw-loader'
      })
    }
  },

  // Buefy/Bulma module configuration
  buefy: {
    // https://buefy.org/documentation/constructor-options here
  },

  // Axios module configuration (https://axios.nuxtjs.org/options)
  axios: {
    // Environment variable API_URL can be used to override baseURL.
    baseURL: 'http://localhost:8000/api/v1/'
  }

}
